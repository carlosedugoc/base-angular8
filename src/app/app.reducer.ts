import { ActionReducer, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import { environment } from '../environments/environment';
import * as fromUI from "./shared/ui.reducers";

export interface AppState {
  ui: fromUI.State
}

export const appReducers: ActionReducerMap<AppState> = {
  ui: fromUI.uiReducer
};


export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
