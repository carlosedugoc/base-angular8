//Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';

//Libraries
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Routing
import { appRouting } from './app.routes';

//Services
import { AppConfig } from './config/config.service';

//Translator
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

//Components
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { CallStationComponent } from './pages/call-station/call-station.component';
import { ZoneReferenceComponent } from "./pages/zone-reference/zone-reference.component";
import { LoginComponent } from './login/login.component';
import { PagesComponent } from './pages/pages.component';
import { StoreModule } from '@ngrx/store';
import { metaReducers, appReducers } from './app.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './app.effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';

//Functions
export function initializeApp(appConfig: AppConfig) {
  return () => appConfig.load();
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const NGRX_IMPORTS = [
  StoreModule.forRoot(appReducers),
  StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
  EffectsModule.forRoot([AppEffects]),
  StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CallStationComponent,
    LoginComponent,
    PagesComponent,
    ZoneReferenceComponent
  ],
  imports: [
    BrowserModule,
    appRouting,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    ...NGRX_IMPORTS
  ],
  providers: [
    AppConfig,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [AppConfig],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
