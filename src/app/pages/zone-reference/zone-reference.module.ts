import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';

import { ZoneReferenceListSearchComponent } from './zone-reference-list-search/zone-reference-list-search.component';
import { ZoneReferenceAddComponent } from './zone-reference-add/zone-reference-add.component';
import { ZoneReferenceMapComponent } from './zone-reference-map/zone-reference-map.component';
import { appRouting } from './zone-reference.routes';
import { ZoneReferenceMapEditComponent } from './zone-reference-map/zone-reference-map-edit.component';

import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    ZoneReferenceListSearchComponent,
    ZoneReferenceAddComponent,
    ZoneReferenceMapComponent,
    ZoneReferenceMapEditComponent,
  ],
  imports: [
    CommonModule,
    appRouting,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCqskADZ1aDpVS45qqdoWWNVQM5muB196Q',
      libraries: ['drawing', 'geometry']
    }),
    FormsModule
  ]
})
export class ZoneReferenceModule { }
