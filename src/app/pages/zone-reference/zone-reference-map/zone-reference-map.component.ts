// import { Component, OnInit } from '@angular/core';
// import { Marcador } from "./zone-reference-map.class";
// declare const google: any;

// @Component({
//   selector: 'app-zone-reference-map',
//   templateUrl: './zone-reference-map.component.html',
//   styles: []
// })
// export class ZoneReferenceMapComponent implements OnInit {
//   marcadores: Marcador[] = []
//   paths = []
//   lat: number = 51.678418;
//   lng: number = 7.809007;
//   // constructor() { }

//   ngOnInit() {
//     if (localStorage.getItem('marcadores')) {
//       this.marcadores = JSON.parse(localStorage.getItem('marcadores'))
//     }
//     if (localStorage.getItem('paths')) {
//       this.paths = JSON.parse(localStorage.getItem('paths'))
//     }
//     console.log(this.paths);
//   }

//   markclick() {
//     console.log('ok');
//   }

//   agregarMarcador(evento: any) {
//     console.log(evento, 'ok')
//     const nuevoMarcador = new Marcador(evento.coords.lat, evento.coords.lng)
//     this.marcadores.push(nuevoMarcador);
//     localStorage.setItem('marcadores', JSON.stringify(this.marcadores))
//   }

//   center: any = {
//     lat: 33.5362475,
//     lng: -111.9267386
//   };

//   onMapReady(map) {
//     console.log(map)
//     this.initDrawingManager(map);
//   }

//   initDrawingManager(map: any) {
//     const options = {
//       drawingControl: true,
//       drawingControlOptions: {
//         drawingModes: ["polygon", "marker"]
//       },
//       polygonOptions: {
//         draggable: true,
//         editable: true,
//       },
//       markerOptions: {
//         icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
//         draggable: true
//       },
//       drawingMode: google.maps.drawing.OverlayType.POLYGON
//     };

//     const drawingManager = new google.maps.drawing.DrawingManager(options);
//     drawingManager.setMap(map);


//     let overlayClickListener = (overlay) => {
//       google.maps.event.addListener(overlay, "mouseup", function (event) {
//         debugger;
//         return overlay.getPath().getArray();
//       });
//     }

//     google.maps.event.addListener(drawingManager, "overlaycomplete", (event) => {
//       var newShape = event.overlay;
//       newShape.type = event.type;
//     });

//     google.maps.event.addListener(drawingManager, "overlaycomplete", (event) => {
//       if (event.type == 'marker') return;
//       overlayClickListener(event.overlay);
//       let paths = []
//       event.overlay.getPath().getArray().map(res => {
//         let path = {
//           lat: res.lat(),
//           lng: res.lng()
//         }
//         paths.push(path)
//       })
//       this.paths.push(paths)
//       localStorage.setItem('paths', JSON.stringify(this.paths))
//       console.log(this.paths)
//     });
//   }

//   savePoligons(){

//   }

//   saveCustomizedMarkers(){

//   }




// }


import { Component, OnInit } from '@angular/core';
import { Marcador } from "./zone-reference-map.class";
declare const google: any;

@Component({
  selector: 'app-zone-reference-map',
  templateUrl: './zone-reference-map.component.html',
  styles: []
})
export class ZoneReferenceMapComponent implements OnInit {
  marcadores: Marcador[] = []
  // marcadorSeleccionado: Marcador = new Marcador(0, 0, '')
  marcadorSeleccionado = {
    marcador: new Marcador(0, 0, ''),
    index: 0
  }
  paths = []
  lat: number = 51.678418;
  lng: number = 7.809007;
  // constructor() { }

  ngOnInit() {
    if (localStorage.getItem('marcadores')) {
      this.marcadores = JSON.parse(localStorage.getItem('marcadores'))
    }
    if (localStorage.getItem('paths')) {
      this.paths = JSON.parse(localStorage.getItem('paths'))
    }
    console.log(this.paths);
  }

  markclick() {
    console.log('ok');
  }

  agregarMarcador(evento: any) {
    console.log(evento, 'ok')
    const nuevoMarcador = new Marcador(evento.coords.lat, evento.coords.lng)
    this.marcadores.push(nuevoMarcador);
    localStorage.setItem('marcadores', JSON.stringify(this.marcadores))
  }

  center: any = {
    lat: 33.5362475,
    lng: -111.9267386
  };

  onMapReady(map) {
    console.log(map)
    this.initDrawingManager(map);
  }

  initDrawingManager(map: any) {
    const options = {
      drawingControl: true,
      drawingControlOptions: {
        drawingModes: ["polygon", "marker"]
      },
      polygonOptions: {
        draggable: true,
        editable: true,
      },
      markerOptions: {
        icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
        draggable: true
      }
      //,drawingMode: google.maps.drawing.OverlayType.POLYGON
    };

    const drawingManager = new google.maps.drawing.DrawingManager(options);
    drawingManager.setMap(map);




    google.maps.event.addListener(drawingManager, "overlaycomplete", (event) => {
      var newShape = event.overlay;
      newShape.type = event.type;
    });

    google.maps.event.addListener(drawingManager, "overlaycomplete", (event) => {
      if (event.type == 'marker') {
        debugger;
        this.saveCustomizedMarkers(event)
      } else {
        this.savePoligons(event)
      }
    });
  }

  savePoligons(event) {
    let overlayClickListener = (overlay) => {
      google.maps.event.addListener(overlay, "mouseup", function (event) {
        debugger;
        return overlay.getPath().getArray();
      });
    }
    overlayClickListener(event.overlay);
    let paths = []
    event.overlay.getPath().getArray().map(res => {
      let path = {
        lat: res.lat(),
        lng: res.lng()
      }
      paths.push(path)
    })
    this.paths.push(paths)
    localStorage.setItem('paths', JSON.stringify(this.paths))
    console.log(this.paths)
  }

  saveCustomizedMarkers(event) {
    debugger
    const nuevoMarcador = new Marcador(event.overlay.getPosition().lat(), event.overlay.getPosition().lng(), event.overlay.icon)
    this.marcadores.push(nuevoMarcador);
    localStorage.setItem('marcadores', JSON.stringify(this.marcadores))
  }

  borrarMarcador(i: number) {
    this.marcadores.splice(i, 1)
    localStorage.setItem('marcadores', JSON.stringify(this.marcadores))
  }

  editarMarcador(marcador: Marcador, index: number) {
    this.marcadorSeleccionado = { marcador, index }
  }

  guardarMarcador(event) {
    this.marcadores.splice(event.index, 1)
    this.marcadores.push(event.marcador)
    localStorage.setItem('marcadores', JSON.stringify(this.marcadores))
  }

}


