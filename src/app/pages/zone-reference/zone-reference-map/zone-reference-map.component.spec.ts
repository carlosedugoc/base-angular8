import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AgmCoreModule } from '@agm/core';

import { ZoneReferenceMapComponent } from './zone-reference-map.component';

describe('ZoneReferenceMapComponent', () => {
  let component: ZoneReferenceMapComponent;
  let fixture: ComponentFixture<ZoneReferenceMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ZoneReferenceMapComponent],
      imports: [
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyCqskADZ1aDpVS45qqdoWWNVQM5muB196Q'
        })]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneReferenceMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
