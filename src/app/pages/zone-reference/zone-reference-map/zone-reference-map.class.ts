export class Marcador {
    public titulo = 'Sin Titulo';
    public desc = 'Sin Descripción';
    constructor(public lat: number, public lng: number, public icon?: string) { }
}