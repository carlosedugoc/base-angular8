import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { } from 'events';

@Component({
  selector: 'app-zone-reference-map-edit',
  templateUrl: './zone-reference-map-edit.component.html',
  styles: []
})
export class ZoneReferenceMapEditComponent implements OnInit {

  @Input() marcadorSeleccionado: any;
  @Output() marcador = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  guardar(){
    this.marcador.emit(this.marcadorSeleccionado)
  }

}
