import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AgmCoreModule } from '@agm/core';

import { ZoneReferenceListSearchComponent } from './zone-reference-list-search.component';
import { ZoneReferenceMapComponent } from '../zone-reference-map/zone-reference-map.component';

describe('ZoneReferenceListSearchComponent', () => {
  let component: ZoneReferenceListSearchComponent;
  let fixture: ComponentFixture<ZoneReferenceListSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ZoneReferenceListSearchComponent, ZoneReferenceMapComponent],
      imports: [
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyCqskADZ1aDpVS45qqdoWWNVQM5muB196Q'
        })]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneReferenceListSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
