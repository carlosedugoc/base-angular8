import { RouterModule, Routes } from '@angular/router';
import { ZoneReferenceListSearchComponent } from './zone-reference-list-search/zone-reference-list-search.component';
import { ZoneReferenceAddComponent } from './zone-reference-add/zone-reference-add.component';
import { ZoneReferenceMapComponent } from './zone-reference-map/zone-reference-map.component';

const routes: Routes = [
    {
        path: 'list-search',
        component: ZoneReferenceListSearchComponent,
        children: [
            { path: 'map', component: ZoneReferenceMapComponent },
        ]
    },
    {
        path: 'zone-reference',
        component: ZoneReferenceAddComponent,
        children: [
            { path: 'map', component: ZoneReferenceMapComponent },
        ]
    }
];

export const appRouting = RouterModule.forChild(routes);