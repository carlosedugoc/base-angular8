import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AgmCoreModule } from '@agm/core';

import { ZoneReferenceAddComponent } from './zone-reference-add.component';
import { ZoneReferenceMapComponent } from '../zone-reference-map/zone-reference-map.component';

describe('ZoneReferenceAddComponent', () => {
  let component: ZoneReferenceAddComponent;
  let fixture: ComponentFixture<ZoneReferenceAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ZoneReferenceAddComponent, ZoneReferenceMapComponent],
      imports: [
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyCqskADZ1aDpVS45qqdoWWNVQM5muB196Q'
        })]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneReferenceAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
