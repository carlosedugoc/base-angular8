import { RouterModule, Routes } from '@angular/router';
import { AnalystHomeComponent } from './analyst-home/analyst-home.component';
import { CoordinatorHomeComponent } from './coordinator-home/coordinator-home.component';
import { ManagerHomeComponent } from './manager-home/manager-home.component';
import { SupervisorHomeComponent } from './supervisor-home/supervisor-home.component';
import { TechnicalHomeComponent } from './technical-home/technical-home.component';

const routes: Routes = [
    { path: 'analyst', component: AnalystHomeComponent },
    { path: 'coordinator', component: CoordinatorHomeComponent },
    { path: 'manager', component: ManagerHomeComponent },
    { path: 'supervisor', component: SupervisorHomeComponent },
    { path: 'technical', component: TechnicalHomeComponent }
];

export const appRouting = RouterModule.forChild(routes);