import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';

@Component({
  selector: 'app-analyst-home',
  templateUrl: './analyst-home.component.html',
  styles: []
})
export class AnalystHomeComponent implements OnInit {
  title : string;
  cargando: boolean
  subscription: Subscription
  constructor(private userService: UserService, private store: Store<AppState>) {
    this.title = this.userService.info
  }

  ngOnInit() {
    this.subscription = this.store.select('ui').subscribe(ui => {
      this.cargando = ui.isLoading
      this.userService.setInfo = this.cargando
    })
  }

}
