import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {
  title: string;
  constructor(private userService: UserService) {
    this.title = this.userService.info
    this.userService.info$.subscribe(res => this.title = res)
  }

  ngOnInit() { }

}
