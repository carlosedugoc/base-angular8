import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalystHomeComponent } from './analyst-home/analyst-home.component';
import { SupervisorHomeComponent } from './supervisor-home/supervisor-home.component';
import { TechnicalHomeComponent } from './technical-home/technical-home.component';
import { CoordinatorHomeComponent } from './coordinator-home/coordinator-home.component';
import { ManagerHomeComponent } from './manager-home/manager-home.component';
import { appRouting } from './home.routes';

@NgModule({
  declarations: [
    AnalystHomeComponent,
    SupervisorHomeComponent,
    TechnicalHomeComponent,
    CoordinatorHomeComponent,
    ManagerHomeComponent
  ],
  imports: [
    CommonModule,
    appRouting
  ]
})
export class HomeModule { }
