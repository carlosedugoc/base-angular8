import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPhoneCallStationComponent } from './add-phone-call-station.component';

describe('AddPhoneCallStationComponent', () => {
  let component: AddPhoneCallStationComponent;
  let fixture: ComponentFixture<AddPhoneCallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPhoneCallStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPhoneCallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
