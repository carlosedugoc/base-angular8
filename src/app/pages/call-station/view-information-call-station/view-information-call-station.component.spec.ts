import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInformationCallStationComponent } from './view-information-call-station.component';

describe('ViewInformationCallStationComponent', () => {
  let component: ViewInformationCallStationComponent;
  let fixture: ComponentFixture<ViewInformationCallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewInformationCallStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewInformationCallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
