import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDirectConnectionCallStationComponent } from './add-direct-connection-call-station.component';

describe('AddDirectConnectionCallStationComponent', () => {
  let component: AddDirectConnectionCallStationComponent;
  let fixture: ComponentFixture<AddDirectConnectionCallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDirectConnectionCallStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDirectConnectionCallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
