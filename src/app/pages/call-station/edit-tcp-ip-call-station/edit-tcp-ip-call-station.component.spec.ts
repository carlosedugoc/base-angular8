import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTcpIpCallStationComponent } from './edit-tcp-ip-call-station.component';

describe('EditTcpIpCallStationComponent', () => {
  let component: EditTcpIpCallStationComponent;
  let fixture: ComponentFixture<EditTcpIpCallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTcpIpCallStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTcpIpCallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
