import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVirtualPhoneCallStationComponent } from './add-virtual-phone-call-station.component';

describe('AddVirtualPhoneCallStationComponent', () => {
  let component: AddVirtualPhoneCallStationComponent;
  let fixture: ComponentFixture<AddVirtualPhoneCallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVirtualPhoneCallStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVirtualPhoneCallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
