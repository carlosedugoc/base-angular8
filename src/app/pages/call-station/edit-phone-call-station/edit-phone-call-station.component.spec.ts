import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPhoneCallStationComponent } from './edit-phone-call-station.component';

describe('EditPhoneCallStationComponent', () => {
  let component: EditPhoneCallStationComponent;
  let fixture: ComponentFixture<EditPhoneCallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPhoneCallStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPhoneCallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
