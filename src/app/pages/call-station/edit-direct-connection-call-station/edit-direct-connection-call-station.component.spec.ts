import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDirectConnectionCallStationComponent } from './edit-direct-connection-call-station.component';

describe('EditDirectConnectionCallStationComponent', () => {
  let component: EditDirectConnectionCallStationComponent;
  let fixture: ComponentFixture<EditDirectConnectionCallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDirectConnectionCallStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDirectConnectionCallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
