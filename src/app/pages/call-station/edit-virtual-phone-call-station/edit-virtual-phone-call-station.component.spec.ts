import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVirtualPhoneCallStationComponent } from './edit-virtual-phone-call-station.component';

describe('EditVirtualPhoneCallStationComponent', () => {
  let component: EditVirtualPhoneCallStationComponent;
  let fixture: ComponentFixture<EditVirtualPhoneCallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVirtualPhoneCallStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVirtualPhoneCallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
