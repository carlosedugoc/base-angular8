import { RouterModule, Routes } from '@angular/router';
import { AddDirectConnectionCallStationComponent } from './add-direct-connection-call-station/add-direct-connection-call-station.component';
import { EditDirectConnectionCallStationComponent } from './edit-direct-connection-call-station/edit-direct-connection-call-station.component';
import { AddPhoneCallStationComponent } from './add-phone-call-station/add-phone-call-station.component';
import { EditPhoneCallStationComponent } from './edit-phone-call-station/edit-phone-call-station.component';
import { AddTcpIpCallStationComponent } from './add-tcp-ip-call-station/add-tcp-ip-call-station.component';
import { EditTcpIpCallStationComponent } from './edit-tcp-ip-call-station/edit-tcp-ip-call-station.component';
import { AddVirtualPhoneCallStationComponent } from './add-virtual-phone-call-station/add-virtual-phone-call-station.component';
import { EditVirtualPhoneCallStationComponent } from './edit-virtual-phone-call-station/edit-virtual-phone-call-station.component';
import { AddVirtualdirectoryCallStationComponent } from './add-virtualdirectory-call-station/add-virtualdirectory-call-station.component';
import { EditVirtualdirectoryCallStationComponent } from './edit-virtualdirectory-call-station/edit-virtualdirectory-call-station.component';
import { ViewInformationCallStationComponent } from './view-information-call-station/view-information-call-station.component';

const routes: Routes = [
    { path: 'add-direct-connection', component: AddDirectConnectionCallStationComponent },
    { path: 'edit-direct-connection', component: EditDirectConnectionCallStationComponent },
    { path: 'add-phone', component: AddPhoneCallStationComponent },
    { path: 'edit-phone', component: EditPhoneCallStationComponent },
    { path: 'add-tcp-ip', component: AddTcpIpCallStationComponent },
    { path: 'edit-tcp-ip', component: EditTcpIpCallStationComponent },
    { path: 'add-virtual-phone', component: AddVirtualPhoneCallStationComponent },
    { path: 'edit-virtual-phone', component: EditVirtualPhoneCallStationComponent },
    { path: 'add-virtual-directory', component: AddVirtualdirectoryCallStationComponent },
    { path: 'edit-virtual-directory', component: EditVirtualdirectoryCallStationComponent },
    { path: 'view-call-station', component: ViewInformationCallStationComponent }
];

export const appCallStationRouting = RouterModule.forChild(routes);