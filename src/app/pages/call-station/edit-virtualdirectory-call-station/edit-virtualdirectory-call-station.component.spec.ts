import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVirtualdirectoryCallStationComponent } from './edit-virtualdirectory-call-station.component';

describe('EditVirtualdirectoryCallStationComponent', () => {
  let component: EditVirtualdirectoryCallStationComponent;
  let fixture: ComponentFixture<EditVirtualdirectoryCallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVirtualdirectoryCallStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVirtualdirectoryCallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
