import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddTcpIpCallStationComponent } from './add-tcp-ip-call-station/add-tcp-ip-call-station.component';
import { EditTcpIpCallStationComponent } from './edit-tcp-ip-call-station/edit-tcp-ip-call-station.component';
import { AddPhoneCallStationComponent } from './add-phone-call-station/add-phone-call-station.component';
import { EditPhoneCallStationComponent } from './edit-phone-call-station/edit-phone-call-station.component';
import { AddDirectConnectionCallStationComponent } from './add-direct-connection-call-station/add-direct-connection-call-station.component';
import { EditDirectConnectionCallStationComponent } from './edit-direct-connection-call-station/edit-direct-connection-call-station.component';
import { ViewInformationCallStationComponent } from './view-information-call-station/view-information-call-station.component';
import { AddVirtualPhoneCallStationComponent } from './add-virtual-phone-call-station/add-virtual-phone-call-station.component';
import { EditVirtualPhoneCallStationComponent } from './edit-virtual-phone-call-station/edit-virtual-phone-call-station.component';
import { AddVirtualdirectoryCallStationComponent } from './add-virtualdirectory-call-station/add-virtualdirectory-call-station.component';
import { EditVirtualdirectoryCallStationComponent } from './edit-virtualdirectory-call-station/edit-virtualdirectory-call-station.component';
import { appCallStationRouting } from './callStation.routes';

@NgModule({
  declarations: [AddTcpIpCallStationComponent, EditTcpIpCallStationComponent, AddPhoneCallStationComponent, EditPhoneCallStationComponent, AddDirectConnectionCallStationComponent, EditDirectConnectionCallStationComponent, ViewInformationCallStationComponent, AddVirtualPhoneCallStationComponent, EditVirtualPhoneCallStationComponent, AddVirtualdirectoryCallStationComponent, EditVirtualdirectoryCallStationComponent],
  imports: [
    CommonModule,
    appCallStationRouting
  ]
})
export class CallStationModule { }
