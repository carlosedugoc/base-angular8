import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVirtualdirectoryCallStationComponent } from './add-virtualdirectory-call-station.component';

describe('AddVirtualdirectoryCallStationComponent', () => {
  let component: AddVirtualdirectoryCallStationComponent;
  let fixture: ComponentFixture<AddVirtualdirectoryCallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVirtualdirectoryCallStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVirtualdirectoryCallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
