import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing'
import { CallStationComponent } from './call-station.component';

describe('CallStationComponent', () => {
  let component: CallStationComponent;
  let fixture: ComponentFixture<CallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CallStationComponent],
      imports: [RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
