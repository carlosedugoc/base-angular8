import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTcpIpCallStationComponent } from './add-tcp-ip-call-station.component';

describe('AddTcpIpCallStationComponent', () => {
  let component: AddTcpIpCallStationComponent;
  let fixture: ComponentFixture<AddTcpIpCallStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTcpIpCallStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTcpIpCallStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
