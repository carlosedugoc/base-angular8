import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(public translate: TranslateService) { }

  switchLanguage(language: string) {
    this.translate.use(language);
  }

}
