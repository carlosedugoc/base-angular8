import { Injectable } from '@angular/core';
import { Subject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  info: any;
  info$: Subject<any>;

  constructor() {
    this.info$ = new Subject<any>();
  }

  set setInfo(valor) {
    this.info = valor
    this.info$.next(valor)
  }


}
