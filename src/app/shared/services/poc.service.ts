import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PocService {

  constructor(private http: HttpClient) { }

  getInfo() {
    return this.http.get('https://54f59e6d-a23b-4991-801a-4e16cd26f3bb.mock.pstmn.io/demo?id=1234');
  }

  postInfo() {
    return this.http.post('https://54f59e6d-a23b-4991-801a-4e16cd26f3bb.mock.pstmn.io/demo', {nombre:"Carlos", apellido:"González"});
  }

}
