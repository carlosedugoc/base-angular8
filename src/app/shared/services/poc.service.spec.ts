import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { PocService } from './poc.service';


describe('PocService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));
  it('should be created', () => {
    const service: PocService = TestBed.get(PocService);
    expect(service).toBeTruthy();
  });
});
