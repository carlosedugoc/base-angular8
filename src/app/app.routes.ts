import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { CallStationComponent } from './pages/call-station/call-station.component';
import { LoginComponent } from './login/login.component';
import { PagesComponent } from './pages/pages.component';
import { ZoneReferenceComponent } from './pages/zone-reference/zone-reference.component';

const routes: Routes = [
    {
        path: '', component: PagesComponent, children: [
            { path: 'home', component: HomeComponent, loadChildren: './pages/home/home.module#HomeModule' },
            { path: 'call-station', component: CallStationComponent, loadChildren: './pages/call-station/call-station.module#CallStationModule' },
            { path: 'zone-reference', component: ZoneReferenceComponent, loadChildren: './pages/zone-reference/zone-reference.module#ZoneReferenceModule' },
        ]
    },
    { path: 'login', component: LoginComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

export const appRouting = RouterModule.forRoot(routes);