import { Component, OnInit } from '@angular/core';
import { GlobalService } from './shared/services/global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'ps-config';

  // constructor(private translate: TranslateService) { }
  constructor(private globalService: GlobalService) { }

  ngOnInit(): void {
    const language = window.navigator.language.substr(0, 2)
    this.globalService.translate.setDefaultLang(language)
  }

}
