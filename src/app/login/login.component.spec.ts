import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { PocService } from '../shared/services/poc.service';
import { UserService } from '../shared/services/user.service';
import { Observable, of } from 'rxjs';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  const servicioPoc = new PocService(null)
  const servicioUser = new UserService()

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    // fixture = TestBed.createComponent(LoginComponent);
    // component = fixture.componentInstance;
    // fixture.detectChanges();
    component = new LoginComponent(servicioUser, servicioPoc)
  });

  it('Se debe consumir el servicio poc por get', () => {
    const animals = `[
      {
          "name": "Meowsy",
          "species": "cat",
          "foods": {
              "likes": [
                  "tuna",
                  "catnip"
              ],
              "dislikes": [
                  "ham",
                  "zucchini"
              ]
          }
      },
      {
          "name": "Barky",
          "species": "dog",
          "foods": {
              "likes": [
                  "bones",
                  "carrots"
              ],
              "dislikes": [
                  "tuna"
              ]
          }
      },
      {
          "name": "Purrpaws",
          "species": "cat",
          "foods": {
              "likes": [
                  "mice"
              ],
              "dislikes": [
                  "cookies"
              ]
          }
      }
  ]`
    spyOn(servicioPoc, 'getInfo').and.callFake(() => {
      return of(animals)
    });
    component.getInfo()
    expect(component.info.length).toBeGreaterThan(0);
  });
});
