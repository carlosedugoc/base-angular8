import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { PocService } from '../shared/services/poc.service';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { ActivarLoadingAction, DesactivarLoadingAction } from '../shared/ui.actions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: ['./LoginComponent.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  cargando: boolean
  subscription: Subscription
  title = "Hi";
  info = []
  constructor(
    private userService: UserService,
    private pocService: PocService,
    private store: Store<AppState>) {
    this.userService.info$.subscribe(res => {
      this.title = res
    })
  }

  ngOnInit() {
    this.subscription = this.store.select('ui').subscribe(ui => {
      this.cargando = ui.isLoading
      this.userService.setInfo = this.cargando
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getInfo() {
    this.pocService.getInfo().subscribe((res: any[]) => {
      this.info = res
      console.log(res)
    })
  }

  postInfo() {
    this.pocService.postInfo().subscribe(res => {
      console.log(res)
    })
  }

  logIn() {
    this.store.dispatch(new ActivarLoadingAction())
    setTimeout(() => {
      this.store.dispatch(new DesactivarLoadingAction())
    }, 10000);
  }

}
